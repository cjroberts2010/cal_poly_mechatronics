"""
@file ElevatorCode.py
@brief Simulates an elevator using user input.
@details This program simulates an elevator as a finite state machine.It takes 
         inputs from the user to decide which floor to go to, and then travels 
         to that floor. Once the floor is reached, the elevator stops and waits.
@author: Christian Roberts
@date: 1/27/2021
@copyright License information here.
https://bitbucket.org/cjroberts2010/cal_poly_mechatronics/src/master/305_HW/ElevatorCode.py
"""
import time
import random

# Function Definitions go here
def motor_cmd(cmd):
    """@brief Commands the motor to move or stop.
    
    @param cmd The command to give the motor    
    """
    if cmd=='UP':   #If we are going up
        print ('Going Up!')
    elif cmd == 'DWN': #If we are going down
        print('Going Down!')
    elif cmd == 'STOP': #If we reach the end
        print('We have Arrived.')
    else: #For any outside commands.
        cmd = 'STOP'
        print('APOCALYPSE')
        #Code should absolutely never hit this point.
        
def Bot_sensor():
    """@brief Randomizes to represent a sensor at the bottom of the shaft."""
    return random.choice([True, False])

def Top_sensor():
    """@brief Randomizes to represent a sensor at the top of the shaft."""    
    return random.choice([True, False])


# Main Program / test program begin
#   This code only runs if the script is executed
#   as main but does not run if the script is imported as a module

if __name__ == "__main__":
    # Program Initialization Begiins
    print ('Welcome to the Elevator! '
           'Please strap in for your elevator adventure.')
    
    state = 0 #Initial State is the Init State
    
    
    while True:
        try:
            #main program goes here
            if state==0:
            #What to do in the initial state: go down, and turn the buttons off.
                motor_cmd('DWN')
                Button_1 = 0
                Button_2 = 0
                n=0
                m=0
                if Bot_sensor(): # Once we hit the bottom, go to state 2 and stop dropping
                    motor_cmd('STOP') #motor stops
                    Button_1 = 0 #Turn the button light off
                    state = 1    #Only then can you claim state 1
                else:
                    pass
                # Run State 0 code
            elif state == 1:
                print ('First Floor!')
                Button_2 = 0
                Button_1 = 0
                n+=1
                if Button_2 == 0:
                    floor = input('Which floor shall I send you to?   ')
                    #User Input to pick which floor to go to
                    if floor.isnumeric():
                        #Only numeric values can be used.
                        floor = int(floor)  #Reassign the int value 
                                    #instead of the string from the input.
                        if floor == 1:  #If the user is on the first floor, 
                                        #and chooses to go on the first floor.
                            Button_1 = 1    #Turn the light on
                        elif floor == 2:    #If the user is on floor 1 and goes
                                            #to floor 2
                            Button_2 = 1    #turn the light on and go to state 2
                            state = 2       #state 2
                            motor_cmd('UP') # Send the Elevator up.
                        else:
                            print('Please select either the first or second floor.')
                            #Apocalypse statement.  Only works if you choose 1 or 2.
                    else:
                        print('I only have number buttons! Pick "1" or "2"')
                    if m == 0 and n == 5:
                        print('You unlock this door with the key of imagination, '
                              'beyond it is another dimension. A dimension of sound, '
                              'a dimension of sight, a dimension of mind. You\'re '
                              'moving into a land of both shadow and substance, of '
                              'things and ideas. You\'ve just crossed over into . . .'
                              ' The Twilight Zone.')
                    else:
                        pass
                # Run State 1 code
            elif state == 2:
                print ('. . .')
                n=0
                m=+1
                if Top_sensor():        #When we reach the top, 
                    state = 3           #Send to State 3.
                    motor_cmd('STOP') # Don't go through the roof
                else:
                    pass# If we aren't at the top, keep going!

            elif state == 3:
                print ('Second Floor!')
                Button_2 = 0
                if Button_2 == 0:
                    floor = input('Which floor shall I send you to?   ')
                    #User Input to pick which floor to go to
                    if floor.isnumeric():
                    #Only numeric values can be used.
                        floor = int(floor)
                        if floor == 1:  #If the user is on the first floor, 
                                        #and chooses to go on the first floor.
                            Button_1 = 1    #Turn the light on
                            state = 4       #Begin to move down.
                            motor_cmd('DWN')
                        elif floor == 2:    #If the user is on floor 2 and goes
                                            #to floor 2
                            Button_2 = 1    #turn the light on and go to state 2
                            state = 3       #state 2
                        else:
                            print('Please select either the first or second floor.')
                                #Apocalypse statement.  Only works if you choose 1 or 2.
                    else:
                        print('I only have number buttons! Pick "1" or "2"')
                        
            elif state == 4:
                print('. . .')
                if Bot_sensor():    #When we reach the bottom
                    motor_cmd('STOP')   #Don't go through the floor!
                    state = 1           #send back to state 1
                else:
                    pass
                # Run State 4 code
            else:
                print('You unlock this door with the key of imagination, '
                      'beyond it is another dimension. A dimension of sound, '
                      'a dimension of sight, a dimension of mind. You\'re '
                      'moving into a land of both shadow and substance, of '
                      'things and ideas. You\'ve just crossed over into . . .'
                      ' The Twilight Zone.')
                # code to run if state number is wrong
                # Program should never hit this
        

            time.sleep(0.5)
        except KeyboardInterrupt:
                # This except block catches Ctrl-C from the keyboard
                # and only broke the while(True) loop when desired
            break
            
    #Program De-Initialization goes here
    print ('Thanks for riding!')