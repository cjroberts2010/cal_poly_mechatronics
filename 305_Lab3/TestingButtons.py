#This is just a test of the emergency broadcast system.
import pyb
import utime

def ButtonRiser(IRQ_src):
    global state, step, veris, Victory, n
    if state == 0:
        pass
    elif state == 1:
        pass
    elif state == 2:
        print('Wait for me to finish my sequence first!')
    elif state == 3:
        if n == 0:
            step = utime.ticks_ms()
            n=1
            t2ch1.pulse_width_percent(100)
        elif n == 1:
            t2ch1.pulse_width_percent(0)
            n=0
            if utime.ticks_ms()>(step+veris+300):
                Victory = 'false'
            elif utime.ticks_ms() < (step +(veris-300)):
                Victory = 'false'
            else:
                Victory = 'true'
            
if __name__ == "__main__":
    #Program Initializiation
    state = 3 #Welcome to the initialization state
    beeps = []
    #Pin and Button Initializations
    ##Defines the pin used.
    pinA5 = pyb.Pin (pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)    #Define the pin to be used.
    ##Define the frequency of a timer for PWM.
    tim2 = pyb.Timer(2, freq = 20000)   #A timer function
    ##A way to call the PWM.
    t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)   #An easy way to call back the PWM mode
    ##A time unit to build patterns from, in milliseconds.
    t_unit = 500
    ##Defines the button used.
    pinC13 = pyb.Pin (pyb.Pin.cpu.C13)  #Define the button input.
    ##Defines the button interrupt
    ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_RISING_FALLING,
                                       pull=pyb.Pin.PULL_NONE, 
                                       callback=ButtonRiser)
    Victory = 'no'
    n=0
    while True:
        try:
            veris = 500
            if Victory == 'false':
                print('Try AGain')
            elif Victory == 'true':
                print('Got it!')
            else:
                pass
        except KeyboardInterrupt:
            break
