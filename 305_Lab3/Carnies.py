"""
@file Carnies.py
@brief A game of Simon Says
@details This interactive game will generate a random sequence of morse code, and the player will have
        to replicate it and match the timing to advance.  It has a variety of difficulties that the player
        can choose.  
        Code Download Here: https://bitbucket.org/cjroberts2010/cal_poly_mechatronics/src/master/305_Lab3/Carnies.py
        State Diagram Here: https://bitbucket.org/cjroberts2010/cal_poly_mechatronics/src/master/305_Lab3/FSMLab3.jpg
        https://youtu.be/-mqUtvFsYes
@author Christian Roberts
@date 2/17/2021
@copyright License info here.

"""
#Import Sections
import pyb
import utime
import random
#Function Definitions
def __isitnumber__(index):
    try:
        int(index)
        return True
    except ValueError:
        return False

def ButtonRiser(IRQ_src):
    """@brief Take user inputs and verifies the timing.
    
    @details This button interrupt callback function allows user inputs in state 3 only.  It 
            checks the time that a user holds the button down, and verifies it against the timing template provided by the sequence.
    @param IRQ_src A callback variable for a button interrupt.
    """
    global state, step, veris, Victory, n, t_unit, buffer
    if state == 0:
        pass
    elif state == 1:
        pass
    elif state == 2:
        print('Wait for me to finish my sequence first!')
    elif state == 3:
        if utime.ticks_ms()>(buffer+100):
            if n == 0:
                step = utime.ticks_ms()
                n=1
                t2ch1.pulse_width_percent(100)
            elif n == 1:
                t2ch1.pulse_width_percent(0)
                n=0
                if utime.ticks_ms()>(step+veris+t_unit):
                    Victory = 'false'
                elif utime.ticks_ms() < (step +(veris-t_unit)):
                    Victory = 'false'
                else:
                    Victory = 'true'
            buffer = utime.ticks_ms()
    

#Main Program
if __name__ == "__main__":
    #Program Initializiation
    ##The state of the function state machine.
    state = 0 #Welcome to the initialization state
    ##The sequence generated.
    beeps = []
    #Pin and Button Initializations
    ##Defines the pin used.
    pinA5 = pyb.Pin (pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)    #Define the pin to be used.
    ##Define the frequency of a timer for PWM.
    tim2 = pyb.Timer(2, freq = 20000)   #A timer function
    ##A way to call the PWM.
    t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)   #An easy way to call back the PWM mode
    ##A time unit to build patterns from, in milliseconds.
    t_unit = 500
    ##Defines the button used.
    pinC13 = pyb.Pin (pyb.Pin.cpu.C13)  #Define the button input.
    ##Defines the button interrupt
    ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_RISING_FALLING,
                                       pull=pyb.Pin.PULL_NONE, 
                                       callback=ButtonRiser)
    ##A string verifying the success or failure of the user input.
    Victory = 'not'
    ##A binary value determining if a button is on its rising or falling edge.
    n = 0
    ##The number of player wins.
    wins = 0
    ##The number of player losses.
    losses = 0
    ##A buffer count to remove any bounce in the button presses.
    buffer = utime.ticks_ms()
    print('Step right up! Come witness our Marvelous Machine Wonder! Outsmart the Mechanical Mind and win a prize!')
    print('Try to make a pattern better than our Machine of Minds!')
    print('Watch the pattern, and push the button to match! Hold down the button longer for a long bleep and shorter for a short blip.')
    while True:
        try:
            #Main Program Code Goes Here
            #Code for state 0 init
            if state == 0:
                t2ch1.pulse_width_percent(0)
                ##The sequence generated.
                beeps = []
                ##The number of beeps to replicate. A higher difficulty makes the game harder.
                difficulty = input('How good do you think you are? Gimme a whole number between 5 and 15.')
                __checker__ = __isitnumber__(difficulty)
                state = 1
                
            if state == 1:
                if __checker__:
                    if int(difficulty) > 15:
                        print('That\'s a hard one! Try an easier number?')
                        state = 0
                    elif int(difficulty) < 5:
                        print('Wow! You want to play the kiddie version? No prizes for that one! Pick a harder one.')
                        state = 0
                    else:
                        if len(beeps) <= int(difficulty)-1:
                            __randomvar__ = random.choice(['long', 'short'])
                            beeps.append(__randomvar__)
                        else:
                            print('Your sequence is ready!')
                            state = 2
                            __idx__ = 0
                            ##Allows the sequence to count from this point.
                            starttime = utime.ticks_ms()
                            t2ch1.pulse_width_percent(100)
                            phase = 1
                else:
                    print('That is not a number silly! Try Again!')
                    
            if state == 2:
                #Display State
                if __idx__ <= phase:
                    if beeps[__idx__] == 'long':
                        ##The value to check the timing against for each beep in the sequence.
                        veris=3*t_unit
                    elif beeps[__idx__] == 'short':
                        veris = t_unit
                    if utime.ticks_ms()>=(starttime+veris):
                        t2ch1.pulse_width_percent(0)
                        if utime.ticks_ms()>=(starttime+veris+t_unit):
                            starttime = utime.ticks_ms()
                            t2ch1.pulse_width_percent(100)
                            __idx__ += 1
                        else:
                            pass
                    else:
                        pass
                elif phase < len(beeps)-1:
                    state = 3
                    print('Your Turn!')
                    t2ch1.pulse_width_percent(0)
                    count = 0
                    if beeps[0] == 'long':
                        veris = 3*t_unit
                    else:
                        veris = t_unit
                else:
                    wins += 1
                    print('Well I gotta give it to ya kid! Seems like you\'ve out-machined our Machine! Why not try it a little harder next time?')
                    print('Wins: ', wins, ' Losses: ', losses)                    
                    state = 0

                    
                    
                
            if state == 3:
                if Victory == 'true':
                    if count<phase:
                        ##The value of the sequence to check in a given loop.
                        count +=1
                        Victory = 'not'
                        if beeps[count] == 'long':
                            veris = 3*t_unit
                        else:
                            veris = t_unit  
                            
                    elif count == phase:
                        if phase < len(beeps):
                            count = 0
                            print('Good Job! Let\'s make it a little harder now, shall we? We ain\'t done yet!')
                            ##The number of beeps from the sequence to display and check in a given round.
                            phase += 1
                            state = 2
                            Victory = 'no'
                            __idx__ = 0
                            t2ch1.pulse_width_percent(0)
                            t2ch1.pulse_width_percent(100)
                        elif phase == len(beeps):
                            wins += 1
                            print('Well I gotta give it to ya kid! Seems like you\'ve out-machined our Machine! Why not try it a little harder next time?')
                            print('Wins: ', wins, ' Losses: ', losses)                    
                            state = 0
                        
                        starttime = utime.ticks_ms()
                elif Victory == 'false':
                    print('It was a good effort, wasn\'t it? Wanna play again?')
                    state = 0
                    losses += 1
                    print('Wins: ', wins, ' Losses: ', losses)
                    Victory = 'no'
                else:
                    pass
            
        except KeyboardInterrupt:
            break
#Program Deinitialization