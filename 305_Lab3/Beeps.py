"""@file beeps.py
@brief Implements a class that defines a beeping sequence.
"""
import random
import utime
import pyb
class MyBeeps:
    
    def __init__(self, length, beeps):
        global pinA5, tim2, t2ch1
        self.length = length
        self.beeps = []
        ##Defines the pin used.
        pinA5 = pyb.Pin (pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)    #Define the pin to be used.
        ##Define the frequency of a timer for PWM.
        tim2 = pyb.Timer(2, freq = 20000)   #A timer function
        ##A way to call the PWM.
        t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)   #An easy way to call back the PWM mode

    def generate(self, length):
        if self.length>=len(self.beeps):
            random_var = random.choice(['long', 'short'])
            self.beeps.append(random_var)
            print('Hello World!')
        else:
            print('Done!')
            
    def lightup(self, beeps):
        global t2ch1, stamp
        for __idx__ in self.beeps:
            stamp = utime.ticks_ms
            if __idx__ == 'long':
                t2ch1.pulse_width_percent(100)
                print('long')
                if utime.ticks_ms()-stamp>=1500:
                    t2ch1.pulse_width_percent(0)
                else:
                    pass
                
                #turn on the light for a long period of time
            elif __idx__ == 'short':
                t2ch1.pulse_width_percent(100)
                print('short')
                if utime.ticks_ms()-stamp>=500:
                    t2ch1.pulse_width_percent(0)
                else:
                    pass
                #Short beeps! 