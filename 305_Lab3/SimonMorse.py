"""@file SimonMorse.py
@brief Creates a game to match a blinking light pattern.

@details TO BE FILLED IN EACH TIME
@author Christian Roberts
@date 2/11/2021
@copyright Licence Info Here.
@image html will include this later, FSM
"""
#Import Sections
import Beeps
import pyb
import utime
#Function Definitions Go Here    
def ButtonScheduler(IRQ_src):
    pass
    
# Main Program / Test Program Begins
if __name__ == "__main__":
    #Program Initialization
    state = 0
    print('Step right up! Step right up! Come try the Amazing Button Timer Game! Winners get nothing, and losers get nothing too!')
    print('Hit the blue button to recreate the pattern shown by our wonderful Mechanical Mind! Get it right, and the patterns get harder! Get it wrong, and well, that\'s the game!')
    print('Hit the blue button to begin!')
    while True:
        try:
            #Main Program Code
            #Code for state zero init
            if state == 0:
                difficulty = 1
                ##Calls the class to create the initial beep sequence.
                sequence = Beeps.MyBeeps(difficulty, [])
                ##Defines the button used.
                pinC13 = pyb.Pin (pyb.Pin.cpu.C13)  #Define the button input.
                ##Timing Initializations
                start = utime.ticks_ms()
                ##Defines the button interrupt callback function.
                ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING,
                                       pull=pyb.Pin.PULL_NONE, 
                                       callback=ButtonScheduler)
                state = 1
            elif state == 1:
                #This is the Generate State.  This will generate a sequence of beeps that is the length of the difficulty value.
                sequence.generate(difficulty)   #We are generating the LED Sequence to run here.
                if len(sequence.beeps)>=difficulty: #Up until we hit our difficulty value,
                    print('Our Mechanical Marvelous Mind has its sequence generated! Are you ready?')
                    state = 2
                    utime.sleep(.08) #### HEY REMOVE THIS SOON YA FOOL ITS JUST A PLACEHOLDER YOOOOOO
            elif state == 2:
                #This is the Display State.  The light show will commence here.  If it is interrupted, an error message will be displayed, and the code will reset.
                sequence.lightup(sequence.beeps)
                state=3
                pass
            elif state == 3:
                #This is the user input State.  Guests will play the game and the difficulty will either go to 1 or get harder.
                for __idx__ in sequence.beeps:
                    start = utime.ticks_ms()
                    
            
            ##This section will use the Nucleo to display the LED Pattern.
            
                
        except KeyboardInterrupt:
            break
        
    #Program Deinitialization Goes Here