"""
@file LEDBlinker.py
@brief Cycle three separate light patterns on user input.
@details This interactive light show will cycle through three different lighting patterns
         when the user requests. It creates and implements a square wave, sine wave, and 
         sawtooth wave pattern and outputs the patterns as an LED Brightnes using PWM.
@author Christian Roberts
@date 2/4/2021
@copyright License info here.
Code Download Here: https://bitbucket.org/cjroberts2010/cal_poly_mechatronics/src/master/305_Lab2/LEDBlinker.py
State Diagram Here: https://bitbucket.org/cjroberts2010/cal_poly_mechatronics/src/master/305_HW/Me305Buttons.pdf
"""

# Function Definitions go here


def ButtonScheduler(IRQ_src):
    """@brief This callback function will change the state variable.
    
    @details This function will change the state variable, sending the program into
    the next state.  If it is at state 1 or 2, it will increase to the next, and if
    it is at state 3, it will go to state 1.  It will also let the user know which state is selected.
    @param IRQ_src A parameter used when the button interrupt feature is used.
    """
    global state, timelast
    print(state)
    timelast = utime.ticks_ms()
    if timelast >= 500:
        if state == 0:
            print('You have selected the Square Wave.')
            state += 1
            return None
        elif state == 1:
            print('You have selected the Sine Wave.')
            state += 1
            return None
        elif state == 2:
            print('You have selected the Sawtooth Wave.')
            state += 1
            return None
        elif state == 3:
            print('You have selected the Square Wave.')
            state = 1
        else:
            pass
    else:
        print('Slow Down! You\'re not racin\' yet!')
    
def State_1_Runs():
    """@brief toggles on and off every 0.5 seconds.
    
    @details This program toggles an LED on and off every half-second.
    """
    global state, timelast
    lapse = utime.ticks_ms()
    diff = utime.ticks_diff(lapse, timelast)
    if diff%1000 == 0:
        t2ch1.pulse_width_percent(100)
    elif diff%500 == 0:
        t2ch1.pulse_width_percent(0)
    else:
        pass
    

def State_2_Runs():
    """@brief Returns a sine wave of a time input.
    
    @details This program uses a time input from the microcontroller and returns a sine value 
    of that time input.  This creates a wave overall that the LED displays.
    """
    global timelast
    lapse = utime.ticks_ms()
    diff = utime.ticks_diff(lapse, timelast)
    var = 50+(50*math.sin(diff*2*math.pi/1000))
    t2ch1.pulse_width_percent(var)
    
    
def State_3_Runs():
    """@brief This program uses a time input to create a sawtooth wave.

    @details This program takes the time input and returns a percent of a specified time lapse,
     which is then used as a pulse width modulation for an LED.
    """
    lapse = utime.ticks_ms()
    diff = utime.ticks_diff(lapse, timelast)
    var = (diff/10)%100
    t2ch1.pulse_width_percent(var)
     
# Main Program / test program begin
#   This code only runs if the script is executed
#   as main but does not run if the script is imported as a module

import pyb
import math
import utime
##Defines the wave selected.
state = 0   #Defines the states to run during initialization.
##Defines the pin used.
pinA5 = pyb.Pin (pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)    #Define the pin to be used.
##Defines the button used.
pinC13 = pyb.Pin (pyb.Pin.cpu.C13)  #Define the button input.
##Defines the button interrupt callback function.
ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING,
                                       pull=pyb.Pin.PULL_NONE, 
                                       callback=ButtonScheduler)    #Should an interrupt occur, run the callback function.
##Define the frequency of a timer for PWM.
tim2 = pyb.Timer(2, freq = 20000)   #A timer function
##A way to call the PWM.
t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)   #An easy way to call back the PWM mode
##Initialization for the timestamp from the last time the button was pushed.
timelast = utime.ticks_ms()
print('Welcome to the Light Fantastique! Please remain seated with your hands, arms feet and legs inside the vehicle. '
      'To use the show controller, hit the Blue Button. Each time you do, we will play our unique light shows. '
      'So sit back, and enjoy the show!')
#Define the light to be lit up and the button to use

if __name__ == "__main__":
    # Program Initialization Begins
    while True:
        try:
            if state == 0:
                pinA5.low()
                
            elif state == 1:
                #Cycle between on and off every half second
                State_1_Runs()
                
                
            elif state == 2:
                State_2_Runs()

            elif state ==3:
                State_3_Runs()

        except KeyboardInterrupt:
            # THis except block catches Ctrl-C from the keyboard
            # and only broke the while(True) loop when desired
            break
            
    #Program De-Initialization goes here
    print ('We hope you have enjoyed Light Fantastique! Please, gather your belongings and head to the nearest exit. '
           'We hope you have a wonderful day!')    
