"""
@file main.py
@brief The finite state machine controlling the Nucleo and communicating with the front end.

@details This file acts as the finite state machine for the Nucleo. It takes inputs from the user 
through serial communication with the front end, and calls on each different state to achieve each 
desired effect. The program starts in state 0, the Read state, and if a command is received from the 
user through serial communication, it will transfer into state 1, the Disperse state. From there, based on the 
command given, this will generate the data requested and return it through serial communication to the front end.
The code can be downloaded here:https://bitbucket.org/cjroberts2010/cal_poly_mechatronics/src/master/305_Lab4/main.py
The finite state machine diagram can be found here:
@author Christian Roberts
@date 3/16/2021
@copyright License info here.

"""

from pyb import UART
import utime
import pyb
import controlTask
import shares

##The UART communication to send information back to the front end.
myuart = UART(2)
##A debugging value that tracks how many commands have been sent.
chars_received = 0
##The state of the finite state machine.
state = 0
##The time, in ms, since the start of the Nucleo.
start_time = utime.ticks_ms()
##An array of values containing the position of motor 1.
position_array1 = []
##An array of values containing the position of motor 2.
position_array2= []
##An array of values containing the timestamps for each motor reading.
times_array= []
##An array of values containing the velocities of motor 1.
velocity_array1 = []
##an array of values containing the velocities of motor 2.
velocity_array2 = []
##An indexing counter.
n=0
##Turn off the REPL in the Nucleo, so as not to send more information than necessary to the PC.
pyb.repl_uart(None)
##An indexing counter.
m=0
##The control task using both motors and both encoders.
controller = controlTask.controlTask(4, pyb.Pin.board.PB6, pyb.Pin.board.PB7, 8,
                         pyb.Pin(pyb.Pin.board.PC6), pyb.Pin(pyb.Pin.board.PC7), pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP),
                         pyb.Pin(pyb.Pin.board.PB4), pyb.Pin(pyb.Pin.board.PB5), 3, pyb.Pin(pyb.Pin.board.PB0), 
                         pyb.Pin(pyb.Pin.board.PB1), 3, 90)
# Check for characters, if one is received respond to the PC with a formatted
# string
##Define State Definitions
state0_read = 0
state1_disperse = 1
state2_generate = 2
state3_position = 3
state4_zero = 4
state5_delta = 5
state6_run = 6
state7_off = 7
state8_velocity = 8
state9_velocity_plot = 9
state10_control=10
while True:
    try:
    
        if state == state0_read:
            if myuart.any():
                # Read one character and turn it into a string
                in_char = myuart.read(1).decode()
                chars_received += 1
                state = state1_disperse
            else:
                if m == 'm':
                    print('WOWie')
                    m=0
                    print('The state is : ' + str(state))
                else:
                    pass
        
        if state == state1_disperse:
            # This will pass us to each state that we can develop depending on the input character.
            if in_char == 'g':
                state = state2_generate
                gotime = utime.ticks_ms()
            elif in_char == 'p':
                state = state3_position
            elif in_char == 'z':
                state = state4_zero
            elif in_char == 'd':
                state = state5_delta
            elif in_char == 'm':
                state = state6_run
            elif in_char == 'o':
                state = state7_off
            elif in_char == 'w':
                state = state8_velocity
            elif in_char == 'v':
                state = state9_velocity_plot
                gotime = utime.ticks_ms()
            elif in_char == 'x':
                state = state10_control
                
            print(state)
            print('state has changed!')
        
        elif state == state2_generate:
            
            if utime.ticks_diff(utime.ticks_ms(), gotime) >= 19: 
                controller.update_position()
                position = controller.get_position()
                position_array1.append(position[0])
                position_array2.append(position[1])
                times_array.append(20*n)
                n+=1
                gotime = utime.ticks_ms()
                print(state)
                print(n)
            elif n>=300:
                for idx in range(len(position_array1)):
                    dataString = '{:}, {:}, {:}, '.format(times_array[idx],position_array1[idx], position_array2[idx])
                    print(dataString)
                    myuart.write(dataString.encode())
                    print('hi')
                
                state = state0_read
                dataString = 'maxed'
                myuart.write(dataString.encode())
                dataString=[]
                times_array = []
                position_array1 = []
                position_array2 = []
                n=0
            else:
                state = state2_generate
                m='m'
            
        
        elif state == state3_position:
            controller.update_position()
            position = 'Position is ' + str(shares.pos1) + ' and position 2 is ' + str(shares.pos2)
            myuart.write(position.encode())
            state = state0_read
    
        elif state == state4_zero:
            controller.zero()
            myuart.write('Zeroed!'.encode())
            state = state0_read
            
        elif state == state5_delta:
            controller.update_delta()
            delta1 = shares.delta1
            delta2 = shares.delta2
            delta_str = 'Delta is ' + str(delta1) + 'and ' + str(delta2)
            myuart.write(delta_str.encode())
            print('Delta Gotten!')
            state = state0_read
        
        elif state == state6_run:
            controller.motor_enable()
            state = state0_read
            myuart.write('Running!'.encode())
            print('MotorON!')
        
        elif state == state7_off:
            controller.kill()
            state = state0_read
            myuart.write('No longer running.'.encode())
            print('MotorOff!')
            
            
        elif state == state8_velocity:
            vel1, vel2 = controller.get_velocity()
            myuart.write('Velocity 1 is: ' + str(vel1) + ' and Velocity 2 is: ' + str(vel2))
            state = state0_read
            print('Velocity gotten!')
        
        elif state == state9_velocity_plot:
            if utime.ticks_diff(utime.ticks_ms(), gotime) >= 19: 
                controller.update_position()
                velocity = controller.get_velocity()
                velocity_array1.append(velocity[0])
                velocity_array2.append(velocity[1])
                times_array.append(20*n)
                n+=1
                gotime = utime.ticks_ms()
                print(state)
                print(n)
            elif n>=100:
                for idx in range(len(velocity_array1)):
                    dataString = '{:}, {:}, {:}, '.format(times_array[idx],velocity_array1[idx], velocity_array2[idx])
                    print(dataString)
                    myuart.write(dataString.encode())
                    print('hi')
                
                state = state0_read
                dataString = 'maxed'
                myuart.write(dataString.encode())
                dataString=[]
                times_array = []
                velocity_array1 = []
                velocity_array2 = []
                n=0
            else:
                state = state9_velocity_plot
                m='m'         
                
        elif state == state10_control:
            __idx__ = 0
            __isbroken__ = True
            while __isbroken__ == True:
                Kp = 0.1
                omega_ref = 1000 #rpm
                omega = controller.get_velocity()
                if omega == [0,0]:
                    pass
                else:
                    L1 = Kp*(omega_ref - omega[0])
                    L2 = Kp*(omega_ref - omega[1])
                    controller.set_duty(L1, L2)
                    __idx__ += 1
                    myuart.write(str(omega).encode())
                    if __idx__ >= 1000:
                        __isbroken__ = False
                        state = state0_read
                    else:
                        pass
                
                
            
        start_time = utime.ticks_ms()
    
                # elif in_char == 's':
                #     for idx in range(counter):
                #         myuart.write(out_string[idx].encode())
                    
                    
                    
                #             val = exp(-chars_received/10)*sin(2*pi/3*chars_received)
                #     out_string = '{:}, {:}\r\n'.format(chars_received, val)
                #     counter += 1
                #     chars_received += 1
                #     print(Encoder1.get_position())
                #     if chars_received == 200:
                #         in_char = 's'
    except KeyboardInterrupt:
        print('------Program Closing!------\r\n')
        break
        