"""
@file controlTask.py
@brief A controller task to initialize and use all of the motors and encoders.
@details This file contains a controller class that will be used to initialize
each of the motors and encoders, as well as provide all control of each, and 
feeding data on each back to the main.py file to transfer back to the front
end.
The code can be downloaded here:https://bitbucket.org/cjroberts2010/cal_poly_mechatronics/src/master/305_Lab4/controlTask.py
@author Christian Roberts
@date 3/16/2021
@copyright License info here.
"""

import MotorDriver
import EncoderDriver
import shares
import utime

class controlTask():

    
    def __init__(self, enctim1, encpinA1, encpinB1, enctim2, encpinA2, encpinB2, 
                 sleep, in1, in2, mottim1, in3, in4, mottim2, duty):
        """Builds both encoders and both motors using pins desired.
    
        @param enctim1 The pyb.Timer object used as the timer in Encoder 1.
        @param encpinA1 The pyb.Pin object used as the first pin in encoder 1.
        @param encpinB1 The pyb.Pin object used as the second pin in Encoder 1.
        @param enctim2 The pyb.Timer object used as the timer in Encoder 2.
        @param encpinA2 The pyb.Pin object used as the first pin in Encoder 2.
        @param encpinB2 The pyb.Pin object used as the second pin in Encoder 2.
        @param sleep The pyb.Pin object used as the nSLEEP pin attached to the motors.
        @param in1 The pyb.Pin object used as the IN1 input to Motor 1.
        @param in2 The pyb.Pin object used as the IN2 input to Motor 1.
        @param mottim1 The pyb.Timer object used as the timer for Motor 1.
        @param in3 The pyb.Pin object used as the IN1 input to Motor 2.
        @param in4 The pyb.Pin object used as the IN2 input to Motor 2.
        @param mottim2 The pyb.Timer object used as the timer for Motor 2.
        @param duty The designated duty cycle for both of the motors.
        """
        
        ##An encoder driver initialized using EncoderDriver
        self.enc1 = EncoderDriver.Encoder(enctim1, encpinA1, encpinB1, 1, 2)
        
        
        ##An encoder driver initialized using EncoderDriver
        self.enc2 = EncoderDriver.Encoder(enctim2, encpinA2, encpinB2, 1, 2)
        print('hello')
        ##a motor initialized using MotorDriver
        self.m1 = MotorDriver.Motors(sleep, in1, in2, mottim1, duty, 1, 2)
        
        ##A motor initialized using MotorDriver
        self.m2 = MotorDriver.Motors(sleep, in3, in4, mottim2, duty, 3, 4)
        print('hi')
        
    def update_position(self):
        """Update the recorded position of the encoders, and store them in shares"""
        self.enc1.update()
        self.enc2.update()
        shares.pos1 = self.enc1.get_position()
        shares.pos2 = self.enc2.get_position()
    
    def update_delta(self):
        """Update the recorded deltas of the encoders and store them in shares."""
        self.enc1.update()
        self.enc2.update()
        shares.delta1 = self.enc1.get_delta()
        shares.delta2 = self.enc2.get_delta()
        
    def zero(self):
        """Zero out the encoders and ensure that all of the values are zero."""
        shares.delta1 = 0
        shares.delta2 = 0
        shares.pos1 = 0
        shares.pos2 = 0
        self.enc1.zero()
        self.enc2.zero()
        # print(self.enc1.enc1val)
        # print(self.enc2.enc1val)
        
    def motor_enable(self):
        """Turn both motors on to the duty cycle specified."""
        self.m1.enable()
        self.m2.enable()
        
    def kill(self):
        """Turn both motors off."""
        self.m1.disable()
        self.m2.disable()
    
    def get_position(self):
        """Returns the positions of the encoders."""
        return [shares.pos1, shares.pos2]
    
    def get_velocity(self):
        """@brief Return the velocities of the motors in RPM.
        @details Take two readings of the encoder with a time delay of 10
        ms, and return the change in position divided by the time delay. This 
        will return the velocity in RPM."""
        self.last_time = utime.ticks_ms()
        self.enc1.update()
        self.enc2.update()
        __idx__ = 3
        while __idx__>2:
            if utime.ticks_diff(utime.ticks_ms, self.last_time) >= 10:
                self.enc1.update()
                self.enc2.update()
                delta1 = self.enc1.get_delta()
                delta2 = self.enc2.get_delta()
                shares.velocity1 = (delta1/2/0.01)*60 #rpm
                shares.velocity2 = (delta2/2/0.01)*60
                __idx__ = 0
            else:
                pass
        
                
        return [shares.velocity1, shares.velocity2]
    
    def set_duty(self, duty1, duty2):
        """Designate the duty of each motor individually.
        @param duty1 The duty cycle of motor 1.
        @param duty2 The duty cycle of motor 2.
        """
        self.m1.set_duty(duty1)
        self.m2.set_duty(duty2)
    
    
    

        