import pyb

class Motors():
    def __init__(self, nSLEEP_pin, IN1_pin, IN2_pin, timer, duty, ch1, ch2):
        """ Creates a motor driver by initializing GPIO
        12 pins and turning the motor off for safety.
        13 @param nSLEEP_pin A pyb.Pin object to use as the enable pin.
        14 @param IN1_pin A pyb.Pin object to use as the input to half bridge 1.
        15 @param IN2_pin A pyb.Pin object to use as the input to half bridge 2.
        16 @param timer A pyb.Timer object to use for PWM generation on
        17 IN1_pin and IN2_pin.
        18 @param duty An integer value between -100 and 100 to use as the duty
        19 cycle of the motor.
        20 @param ch1 An integer representing the timer channel to use for 
        21 the first pin.
        22 @param ch2 An integer representing the timer channel to use for 
        23 the second pin.
        24 The code can be found here: https://bitbucket.org/cjroberts2010/cal_poly_mechatronics/src/master/305_Lab4/MotorDriver.py
        """    
        self.nSLEEP_pin = nSLEEP_pin
        self.IN1_pin = IN1_pin
        self.IN2_pin = IN2_pin
        self.timer = pyb.Timer(timer, freq = 20000)
        self.duty = duty
        self.t1ch1 = self.timer.channel(ch1, pyb.Timer.PWM, pin=self.IN1_pin)
        self.t1ch2 = self.timer.channel(ch2, pyb.Timer.PWM, pin=self.IN2_pin)
        print('Creating a Motor Driver.')
        
    def enable(self):
        """This method will enable both motors based on the duty specified. 
        If the duty cycle specified is positive, the motor will spin counterclockwise,
        while the opposite is true if the duty cycle specified is negative.
        """
        self.nSLEEP_pin.high()
        print('Enabling')
        if self.duty > 0:
            self.t1ch1.pulse_width_percent(0)
            self.t1ch2.pulse_width_percent(self.duty)
        elif self.duty < 0:
            self.t1ch2.pulse_width_percent(self.duty)
            self.IN1_pin.low()
        elif self.duty == 0:
            print('Try a value that isn\'t 0!')
            
    def disable(self):
        """This method will simply disable both motors."""
        
        print('Disabling')
        self.nSLEEP_pin.low()
        if self.duty>0:
            self.t1ch2.pulse_width_percent(0)
        elif self.duty<=0:
            self.t1ch1.pulse_width_percent(0)
        
        
        
    def set_duty (self, duty):
        '''This method sets the duty cycle to be sent to the motor to the given level.
        Positive values cause integer holding the duty cycle of the PWM signal sent to the motor.'''
        print('New Duty is {:}'.format(duty))
        self.duty = duty
        

# if __name__ == '__main__':
#     sleeper = pyb.Pin(pyb.Pin.cpu.A15, mode=pyb.Pin.OUT_PP)
#     timer = pyb.Timer(3, freq=20000)
#     B4 = pyb.Pin(pyb.Pin.cpu.B4, pyb.Pin.OUT_PP)
#     B5 = pyb.Pin(pyb.Pin.cpu.B5, pyb.Pin.OUT_PP)
#     duty = 90
#     mot1 = Motors(sleeper, B4, B5, timer, duty)
#     mot1.enable()
#     mot1.set_duty(50)
    