# -*- coding: utf-8 -*-
"""
Created on Thu Feb 18 08:41:50 2021

@author: cjrob
"""

import serial
ser = serial.Serial(port='COM13', baudrate=115273, timeout=1)

def sendChar():
    inv = input('Give me a character!   ')
    ser.write(str(inv).encode('ascii'))
    myval = ser.readline().decode('ascii')
    return myval

for n in range(1):
    print(sendChar())
    
ser.close()