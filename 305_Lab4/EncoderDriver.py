##This will be a class for the encoder driver.
import pyb

class Encoder():
    
    def __init__(self, timer, pinA, pinB, ch1, ch2):
        """Build the encoder to be used.
        Code can be found here: https://bitbucket.org/cjroberts2010/cal_poly_mechatronics/src/master/305_Lab4/EncoderDriver.py
        @param timer The pyb.Timer object to use as a timer.
        @param pinA The pyb.Pin object to use as the first pin of the encoder.
        @param pinB The pyb.Pin object to use as the second pin of the encoder.
        @param ch1 An integer representing the timer channel of the first pin.
        @param ch2 An integer representing the timer channel of the second pin.
        """
        ##Initialize the timer, using the given timer.
        self.timer = pyb.Timer(timer, period = 0xffff, prescaler = 0)
        ##Initialize Channel 1 for the timer, using the given pin.
        self.tch1 = self.timer.channel(ch1, pyb.Timer.ENC_A, pin=pinA)
        ##Initialize Channel 2 for the timer, using the given pin.
        self.tch2 = self.timer.channel(ch2, pyb.Timer.ENC_B, pin=pinB)
        ##The stored values of the encoder.
        self.enc1val = [0,0]
        ##The actual position of the encoder
        self.position = 0
        
    def update(self):
        """Update to the most recent position of the encoder."""
        self.enc1val[0] = self.enc1val[1]
        self.enc1val[1] = self.timer.counter()
        self.delta = self.enc1val[1] - self.enc1val[0]
        if self.delta>=0xffff/0x2:
            self.delta -= 0xffff
            self.position += self.delta
        elif self.delta <= -0xffff/0x2:
            self.delta += 0xffff
            self.position += self.delta
        else:
            self.position += self.delta
            
            
    def get_position(self):
        """For debug purposes, return the value of the position."""
        ##For debug purposes, print the enc value
        return(self.position)
    
    def get_delta(self):
        """For debug purposes, return the value of the delta."""
        return(self.delta)
    
    def zero(self):
        """Set all values of the encoder to zero, to reset the counter."""
        self.position = 0
        self.enc1val = [0,0]
        self.timer.counter(0)
        
    
    
        
    
    
    