# -*- coding: utf-8 -*-
"""
@file shares.py
@brief An information repository on the Nucleo, accessible to all files.
@details Code download here:https://bitbucket.org/cjroberts2010/cal_poly_mechatronics/src/master/305_Lab4/shares.py
    

@author: Christian Roberts
@date 3/17/2021
@copyright License info here.
"""

pos1 = 0
pos2 = 0
delta1 = 0
delta2 = 0
velocity1 = 0
velocity2 = 0

