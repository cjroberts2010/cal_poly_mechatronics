
"""
@file FrontEnd.py

@brief The UX interface to control the motor and get data.

@details This program will send commands serially to the pyboard which will in turn control the motor.
        Included in this program is a method to graph the results over a set period.
        The commands that can be inserted are as follows:
            g: Generates data over a set period
            q: Quits the serial communication
            d: returns the delta value of the encoder
            p: returns the positions of the encoders
            z: Zeroes the positions and deltas of the encoders.
            v: Plots the outputs of the Generate and Velocity plot phases.
            m: Turns the motors on.
            o: Turns the motors off.
            w: returns the current angular velocity of the motor, in rpm.
            vp: generates velocity data over a set period.
            x: Matches a preloaded reference velocity profile.
        To use this program, simply run the file and when prompted, input the command of choice.
        A finite state machine diagram of this file can be found here:
        The code can be downloaded here:https://bitbucket.org/cjroberts2010/cal_poly_mechatronics/src/master/305_Lab4/FrontEnd.py
@author Christian Roberts
@date 3/16/2021
@copyright License info here.        
"""

import serial
import matplotlib.pyplot as plt

def getData():
    """@brief Run the generate state on the Nucleo.
    
    @details This function will take the user input and run the Generate stage on the Nucleo,
            returning the values of the encoders as a string, to be formatted.
            
    """
    #Write in the serial line the character G as ascii.
    ser.write('g'.encode())
    #reset the timer for each run.
    timeout = 0
    #as long as there are no characters to return in waiting.
    while ser.in_waiting == 0:
        timeout += 1
        #if the timeout gets too long,
        if timeout>6_000_000:
            return None
    #Otherwise, return the character value of the entire line. it will be stored in a string.
    return ser.readline().decode()

def getDelta():
    """@brief Run the delta state on the Nucleo.
    
    @details This function will take the user input and run the delta stage on the Nucleo,
            returning the deltas of the encoders as a string.
            
    """
    ser.write('d'.encode())
    timeout = 0
    while ser.in_waiting == 0:
        timeout += 1
        if timeout>1_000_000:
            return None
    return ser.readline().decode()

def getPosition():
    """@brief Run the position state on the Nucleo.
    
    @details This function will take the user input and run the Position stage on the Nucleo,
            returning the position values as a string, to be formatted.
            
    """
    ser.write('p'.encode())
    timeout = 0
    while ser.in_waiting == 0:
        timeout += 1 
        if timeout> 1_000_000:
            return None
    return ser.readline().decode()

def getZeroed():
    """@brief Run the zero state on the Nucleo.
    
    @details This function will serially write a command to zero the encoders on the nucleo, and return a confirmation.
            
    """
    ser.write('z'.encode())
    timeout = 0
    while ser.in_waiting == 0:
        timeout += 1 
        if timeout> 1_000_000:
            return None
    return ser.readline().decode()

def MotorRun():
    """@brief Enable the motor on the Nucleo.
    
    @details This function will tell the motor to run at the predetermined level.
            
    """ 
    ser.write('m'.encode())
    timeout = 0
    while ser.in_waiting == 0:
        timeout += 1
        if timeout> 1_000_000:
            return None
    return ser.readline().decode()

def MotorOff():
    """@brief Disable the motor on the Nucleo.
    
    @details This function will command the motor to turn off.
            
    """
    ser.write('o'.encode())
    timeout = 0
    while ser.in_waiting == 0:
        timeout += 1
        if timeout> 1_000_000:
            return None
    return ser.readline().decode()

def get_velocity():
    """@brief Run the velocity state on the Nucleo.
    
    @details This function will take the user input and run the velocity stage on the Nucleo,
            returning the velocity values as a string, to be formatted.
            
    """
    ser.write('w'.encode())
    timeout = 0
    while ser.in_waiting == 0:
        timeout += 1
        if timeout> 1_000_000:
            return None
    return ser.readline().decode()

def get_vel_plot():
    '''@brief Run the generate state on the Nucleo.
    
    @details This function will take the user input and run the Generate stage on the Nucleo,
            returning the values of the encoders as a string, to be formatted.
            
    '''
    ser.write('vp'.encode())
    timeout = 0
    while ser.in_waiting == 0:
        timeout += 1
        if timeout> 6_000_000:
            return None
    return ser.readline().decode() 

def Control():
    """@brief Run the Control state on the Nucleo.
    
    @details This function will send the control function to the Nucleo, which will try to match a predetermined 
            reference velocity profile.
            
    """
    ser.write('x'.encode())
    timeout=0
    while ser.in_waiting == 0:
        timeout+=1
        if timeout>10_000_000:
            return None
    return ser.readline().decode()


with serial.Serial(port='COM11', baudrate=115273, timeout=1) as ser:
    while True:
        ##Allow the user to input their action, to either get data or quit.
        user_input = input(' Enter \'g\' to get data, \n or \'q\' to quit \n or \'p\' to get position, \n or \'d\' to get delta \n or \'v\' to get plot\n or \'z\' to zero.\r \n>>')
        if user_input == 'g' or user_input == 'G':
            print('Data Incoming!')
            if ser.readline().decode != 'maxed':
                ##call the function above.
                dataString = getData()
                print('The string is: ' + dataString)

                
                ##Remove line endings
                strippedString = dataString.strip()

                ##Split into two values at the commas
                splitStrings = dataString.split(', ')
               
            
        elif user_input == 'q' or user_input == 'Q':
            print('Data Collection Complete!')
            motoroff = MotorOff()
            ser.close()
            break
        
        elif user_input == 'd' or user_input == 'D':
            delta_str = getDelta()
            print('Delta is: {:}'.format(delta_str))
        
        elif user_input == 'p' or user_input == 'P':
            position_str = getPosition()
            print(position_str)
            
        elif user_input == 'z' or user_input == 'Z':
            Zero_str = getZeroed()
            print(Zero_str)
        
        elif user_input == 'v' or user_input == 'V':
            print('Plot Incoming!')
            time_array = []
            positionarray1 = []
            positionarray2 = []
            for idx in range(int(len(splitStrings)/30)):
                time_array.append(float(splitStrings[idx*30]))
                positionarray1.append(float(splitStrings[idx*30+1]))
                positionarray2.append(float(splitStrings[idx*30+2]))
            fig = plt.subplots(1,2)
            print(time_array)
            plt.subplot(1,2,1)
            plt.plot(time_array, positionarray1)
            plt.subplot(1,2,2)
            plt.plot(time_array, positionarray2)
            plt.xticks(rotation=90)
            plt.show()
            print('plot complete!')
            
        elif user_input == 'm' or user_input == 'M':
            runString = MotorRun()
            print(runString)
            
        elif user_input == 'o' or user_input == 'O':
            runString = MotorOff()
            print(runString)
            
        elif user_input == 'w' or user_input == 'W':
            velString = get_velocity()
            print(velString)
            
        elif user_input == 'vp':
            print('Data Incoming!')
            if ser.readline().decode != 'maxed':
                #call the function above.
                dataString = get_vel_plot()
                print('The string is: ' + dataString)

                
                #Remove line endings
                strippedString = dataString.strip()

                #Split into two values at the commas
                splitStrings = dataString.split(', ')

                
                #Print the value collected.
                
        elif user_input == 'x':
            print('Controlling')
            control = Control()
            print(control)
            