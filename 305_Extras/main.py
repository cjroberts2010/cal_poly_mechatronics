''' Example script for Nucleo backend in two-way comms with Nucleo '''

from pyb import UART, repl_uart
from math import exp, sin, pi

myuart = UART(2)
repl_uart(None)
chars_received = 0

# Check for characters, if one is received respond to the PC with a formatted
# string
while True:
    if myuart.any():
        # Read one character and turn it into a string
        in_char = myuart.read(1).decode()
        chars_received += 1
        
        # If the character was a 'g' respond with a formatted string encoded as
        # a bytearray
        if in_char == 'g':
            val = exp(-chars_received/10)*sin(2*pi/3*chars_received)
            out_string = '{:}, {:}\r\n'.format(chars_received, val)
            myuart.write(out_string.encode())
            print('Data Received')