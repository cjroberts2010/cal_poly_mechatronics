"""@file fib.py
   @brief Define a recursive and a bottom up way to calculate the Fibonacci Sequence.
   @details This program uses two different methods to calculate the Fibonacci Sequence.
            The first method uses a recursive method, calculating each value by 
            individually performing each calculation. The second method stores two 
            numbers in two different variables, and adds them together to replace one of 
            the two.
            https://bitbucket.org/cjroberts2010/cal_poly_mechatronics/src/master/305_lab1/fib.py
   @author Christian Roberts
   @date 1/19/2021
   @copyright License information here.

"""

def fib (idx):
    """
   @brief    this function calculates a Fibonnacci number at a specific index.
   @details    this function calculates from the top down recursively, 
   recalculating the values every time it goes through.
   @param idx An integer specifying the index of the desired Fib number
    """
    if (int(idx) == 0):
        return 0
    elif (int(idx) == 1):
        return 1
    else:
        return fib(int(idx)-1) + fib(int(idx)-2)

def faster_fibs (idx):
    """
       @brief    this function calculates a Fibonacci number at a specific index.
       @details    this function will calculate the Fibonaacci sequence by adding
       values to a list, and using the last two values of the list to calculate 
       the next ones.
       @param idx An integer specifying the index of the desired Fib number
       """
    if idx == '0':
        return 0        #Set the first value manually
    elif idx == '1':
        return 1        #Set the second value manually
    elif int(idx) > 1 and idx.isnumeric(): #For numbers larger than 2
        n = 0
        m = 1
        x = 1
        while x <= int(idx):
            if (x%2) == 0:      #If index x is even,
                n += m          #replace n with the next value in Fib
            else:   
                m += n          #or replace m with the next value. alternate.
            x += 1
        if n>=m:
            return(n)
        else:
            return m
            
    else:
        print('Please enter a positive integer.')    
    

    
if __name__ == '__main__':
    print('Welcome to the Fibonacci Calculator! Enter a'
          ' number as an index, and I will return the value'
          ' of the Fibonacci sequence.  Press Ctrl-C to leave'
          ' at any time.')
    while True:
       idx = (input('Which index are you looking for?  '))

            
       if idx.isnumeric():    
               print('The Fibonacci number at index {:}'
                  ' is {:}'.format(idx,faster_fibs(idx)))
       else:
               print('Please enter a positive integer.')  